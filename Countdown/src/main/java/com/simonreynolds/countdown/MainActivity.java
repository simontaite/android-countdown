package com.simonreynolds.countdown;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.DurationFieldType;
import org.joda.time.Period;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends Activity {

    private Calendar target = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        target = GregorianCalendar.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void showDatePickerDialog(View v) {
        DatePickerDialog datePicker = new DatePickerDialog(this, OnDateSet, target.get(Calendar.YEAR), target.get(Calendar.MONTH), target.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    private void ShowTimePickerDialog() {
        TimePickerDialog timePicker = new TimePickerDialog(MainActivity.this, OnTimeSet, target.get(Calendar.HOUR_OF_DAY), target.get(Calendar.MINUTE), true);
        timePicker.show();
    }

    public void startCountdownClick(View v) {
        long millisecondsInCountdown = Math.abs(target.getTimeInMillis() - GregorianCalendar.getInstance().getTimeInMillis());

        new CountDownTimer(millisecondsInCountdown, 1000) {

            private TextView countdown = (TextView) findViewById(R.id.DisplayCountdown);

            public void onTick(long millisUntilFinished) {
                String result = ToReadableString(new org.joda.time.Period(millisUntilFinished));
                countdown.setText("Time remaining: " + result);
            }

            public void onFinish() {
                countdown.setText("Done!");
            }
        }.start();
    }

    private String ToReadableString(Period period) {

        int days = period.get(DurationFieldType.days());

        int hours = period.get(DurationFieldType.hours());
        int minutes = period.getMinutes();
        int seconds = period.getSeconds();

        StringBuilder sb = new StringBuilder();

        if (days > 0) {
            sb.append(days);
            if (days == 1) {
                sb.append(" day, ");
            } else {
                sb.append(" days, ");
            }
        }

        if (hours > 0) {
            sb.append(hours);
            if (hours == 1) {
                sb.append(" hour, ");
            } else {
                sb.append(" hours, ");
            }
        }

        if (minutes > 0) {
            sb.append(minutes);
            if (minutes == 1) {
                sb.append(" minute, ");
            } else {
                sb.append(" minutes, ");
            }
        }

        if (seconds > 0) {
            sb.append(seconds);
            if (seconds == 1) {
                sb.append(" second, ");
            } else {
                sb.append(" seconds, ");
            }
        }

        String result = sb.toString().trim();

        if (result.endsWith(","))
            result = result.substring(0, result.length() - 1);

        return result;
    }

    private DatePickerDialog.OnDateSetListener OnDateSet = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            target.set(year, month, dayOfMonth);

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ShowTimePickerDialog();
                }
            });
        }
    };

    private TimePickerDialog.OnTimeSetListener OnTimeSet = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
            target.set(Calendar.HOUR_OF_DAY, hourOfDay);
            target.set(Calendar.MINUTE, minute);
            target.set(Calendar.SECOND, 0);

            UpdateTargetDisplay();
        }
    };

    private void UpdateTargetDisplay() {
        if (target.before(GregorianCalendar.getInstance())) {
            AlertDialog alert = new AlertDialog.Builder(getBaseContext()).create();
            alert.setTitle("Error");
            alert.setMessage("Target cannot be in the past");
            alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    target = Calendar.getInstance();
                }
            });
        }

        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                TextView chosenTarget = (TextView) findViewById(R.id.ChosenDateTime);
                String d = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(target.getTime());
                chosenTarget.setText(d);
            }
        });
    }
}
